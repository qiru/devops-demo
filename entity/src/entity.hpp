#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <array>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <memory>
#include <stdint.h>

namespace entity {

namespace asio = boost::asio;
using tcp = asio::ip::tcp;

class Server {
public:
  explicit Server(asio::io_context&, uint16_t);

private:
  void do_accept();

  tcp::acceptor a_;
};

class Session : public std::enable_shared_from_this<Session> {
public:
  explicit Session(tcp::socket&&);

  void start();

private:
  void do_read();
  void do_write(size_t);

  tcp::socket s_;
  std::array<uint8_t, 1024> buf_;
};

}

#endif // ENTITY_HPP
