#include "entity.hpp"

static boost::asio::io_context io{};

int main()
{
  auto server = entity::Server{io, 9999};
  io.run();
  return 0;
}
