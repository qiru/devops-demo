#include "entity.hpp"
#include <boost/asio/buffer.hpp>
#include <boost/asio/write.hpp>
#include <iostream>

using namespace std;

namespace entity {

Server::Server(asio::io_context& io, uint16_t port)
  : a_{io, tcp::endpoint{tcp::v4(), port}} {
  do_accept();
}

void Server::do_accept()
{
  a_.async_accept([this](auto&& ec, auto&& sock) {
    if (ec) {
      cerr << ec.message() << "\n";
    } else {
      make_shared<Session>(move(sock))->start();
    }
    do_accept();
  });
}

Session::Session(tcp::socket&& sock) : s_{move(sock)} {}

void Session::start()
{
  do_read();
}

void Session::do_read()
{
  s_.async_read_some(asio::buffer(buf_), [self = shared_from_this()](auto&& ec, auto len) {
    if (ec) {
      cerr << ec.message() << "\n";
      return;
    }
    self->do_write(len);
  });
}

void Session::do_write(size_t len)
{
  asio::async_write(s_, asio::buffer(buf_, len), [self = shared_from_this()] (auto&& ec, auto) {
    if (ec) {
      cerr << ec.message() << "\n";
      return;
    }
    self->do_read();
  });
}

}
