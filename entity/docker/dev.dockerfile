FROM alpine:3.16

RUN apk add --no-cache g++ cmake make boost-dev boost-system boost-unit_test_framework
