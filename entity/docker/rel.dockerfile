FROM alpine:3.16

ENV SRC_DIR /tmp/src
ENV BUILD_DIR /tmp/build
ADD CMakeLists.txt ${SRC_DIR}/CMakeLists.txt
ADD src ${SRC_DIR}/src

RUN apk add --no-cache g++ cmake make boost-dev boost-system && \
  cmake -D CMAKE_BUILD_TYPE=MiniSizeRel -D CMAKE_INSTALL_PREFIX=/usr/local \
  -D BUILD_TEST=OFF -B "${BUILD_DIR}" "${SRC_DIR}" && \
  cmake --build "${BUILD_DIR}" -j "$(nproc)" && \
  cmake --build "${BUILD_DIR}" --target install/strip && \
  apk del --no-cache g++ cmake make && \
  apk add --no-cache libstdc++ && \
  rm -fr "${BUILD_DIR}" "${SRC_DIR}"
